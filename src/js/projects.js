"use strict";

import Swiper from "swiper";

let swipers =document.querySelectorAll(
    ".content-projects-item__slider"
);

swipers.forEach(container => {
    let arrowLeft = container.querySelector("#arrow-left");
    let arrowRight = container.querySelector("#arrow-right");
    let swiper = new Swiper(container , {
        wrapperClass: "content-projects-item__slider-wrapper",
        slideClass: "content-projects-item__slide",
        loop: true,
        navigation: {
            nextEl: arrowRight,
            prevEl: arrowLeft,
        },
        pagination: {
            el: '.content-projects-item__dots',
            type: 'bullets',
            dynamicBullets: true,
            dynamicMainBullets: 3,
            clickable: true,
            bulletClass: "content-projects-item__dot",
            bulletActiveClass: "content-projects-item__dot_active"
          },
    });
});

