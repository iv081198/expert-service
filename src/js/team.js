"use strict";

ymaps.ready(init);
function init(){
    // Создание карты.
    let mapZoom = 3;

    if(window.matchMedia("(max-width: 756px)").matches) {
        mapZoom = 2;
    }
    console.log(mapZoom);
    let myMap = new ymaps.Map("ymap", {
        // Координаты центра карты.
        // Порядок по умолчанию: «широта, долгота».
        // Чтобы не определять координаты центра карты вручную,
        // воспользуйтесь инструментом Определение координат.

        center: [48.5517362041292,66.9044335],
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: mapZoom
    });

    myMap.behaviors.disable(['scrollZoom', 'multiTouch']);

    myMap.geoObjects.add(new ymaps.Placemark(
        [64.570968, 11.966377], {
        iconCaption: 'Норвегия'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    myMap.geoObjects.add(new ymaps.Placemark(
        [62.723206, 25.933922], {
        iconCaption: 'Финляндия'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    myMap.geoObjects.add(new ymaps.Placemark(
        [62.113107, 15.088463], {
        iconCaption: 'Швеция'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    myMap.geoObjects.add(new ymaps.Placemark(
        [56.142676, 9.466572], {
        iconCaption: 'Дания'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    myMap.geoObjects.add(new ymaps.Placemark(
        [53.139499, -1.685177], {
        iconCaption: 'Великобритания'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    myMap.geoObjects.add(new ymaps.Placemark(
        [49.815317, 6.086365], {
        iconCaption: 'Люксембург'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    myMap.geoObjects.add(new ymaps.Placemark(
        [23.602744, 54.057083], {
        iconCaption: 'Объединенные Арабские Эмираты'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    myMap.geoObjects.add(new ymaps.Placemark(
        [1.323950, 103.791806], {
        iconCaption: 'Сингапур'
    }, {
        preset: 'islands#yellowStarCircleIcon'
    }))

    //myMap.setBounds(myMap.geoObjects.getBounds());
    myMap.margin.setDefaultMargin(50);
}

