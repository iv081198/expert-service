"use strict";

import Imask from "imask";

/**
 * Simple class fo creating
 * materialize inputs and textareas
 *
 * @class MaterialInput
 */
class MaterialInput {
    /**
     *Creates an instance of MaterialInput.
     * @param {*} element
     * @param {*} options
     * @memberof MaterialInput
     */
    constructor(element, options) {
        this.element = element;
        this.options = options;
        this.mask = undefined;
        this.addEvents(element);

        if(options.textarea) {
            this.value = element.querySelector("textarea").value;
            this.addAutoResize();
        } else {
            this.value = element.querySelector("input").value;
        }

        if(this.value) {
            this.updateValue(this.value);
        }

        if(options.phone) {
            this.addPhoneMask();
        }
    }

    addEvents(el) {
        el.addEventListener(
            "input",
            event => this.updateValue(event.target.value)
        );
    }

    addPhoneMask() {
        let input = this.element.querySelector("input");
        this.mask = Imask(input, {
            mask: '+{7}(000)000-00-00'
        })
    }

    addAutoResize() {
        let textarea = this.element.querySelector("textarea");
        textarea.addEventListener(
            "input",
            event => this.autoResize(textarea)
        );
        window.addEventListener(
            "resize",
            event => this.autoResize(textarea)
        );
    }

    autoResize(element) {
        let outerHeight = parseInt(
            window.getComputedStyle(element).height,
            10
        );

        let diff = outerHeight - element.clientHeight;
        element.style.height = "auto";
        element.style.height = element.scrollHeight + diff + 'px';
    }

    updateValue(value) {
        if(value) {
            this.element.classList.add(this.options.activeClass);
        } else {
            this.element.classList.remove(this.options.activeClass);
        }
    }
}

export default MaterialInput;

