"use strict";

import Swiper from "swiper";

let swiper = new Swiper(".body__swiper" , {
    wrapperClass: "body__swiper-wrapper",
    slideClass: "body__bg",
    loop: true,
    effect: "fade",
    slidesPerView: 1,
    speed: 1000,
    autoplay: {
        delay: 3000,
    },
    // pagination: {
    //     el: '.content-projects-item__dots',
    //     type: 'bullets',
    //     dynamicBullets: true,
    //     dynamicMainBullets: 3,
    //     clickable: true,
    //     bulletClass: "content-projects-item__dot",
    //     bulletActiveClass: "content-projects-item__dot_active"
    //   },
});